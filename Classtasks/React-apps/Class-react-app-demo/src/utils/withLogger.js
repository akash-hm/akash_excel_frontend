import React from 'react'

const withLogger = (WrappedComponent) => {
  return class extends React.Component{
    componentDidMount(){
        console.log(`Component ${WrappedComponent.name} Mounted`)
    }

    render(){
        return 
    }
  };
}

export default withLogger