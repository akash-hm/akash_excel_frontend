import './App.css';
import ReducerComponent from './components/ReducerComponent'

function App(){
  return(
    <div>
      <ReducerComponent/>
    </div>
  )
}

export default App;