import React from 'react'

function Welcome({name,age,nationality}) {
  return (
    <div>
        <h1>
            Welcome, {name} and age is {age} and he is from {nationality}.
        </h1>
    </div>
  )
}

export default Welcome