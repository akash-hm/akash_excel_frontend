import React from 'react'



// function Table(){
//     return(
//         <div>
//             <table>
//                 <tr>
//                     <th>ID</th>
//                     <th>Name</th>
//                     <th>Age</th>
//                     <th>Phone</th>
//                     <th>Address</th>
//                 </tr>
//                 <tr>
//                     <td>1</td>
//                     <td>Default</td>
//                     <td>23</td>
//                     <td>1-10</td>
//                     <td>Unknown</td>
//                 </tr>
//             </table>
//         </div>
//     )
// }

const Table = () => {
    return(
        <div>
            <table>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Phone</th>
                    <th>Address</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Default</td>
                    <td>23</td>
                    <td>1-10</td>
                    <td>Unknown</td>
                </tr>
            </table>
        </div>
    )
}


export default Table;