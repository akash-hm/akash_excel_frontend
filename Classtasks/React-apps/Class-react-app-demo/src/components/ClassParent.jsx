import React,{Component} from 'react'


class ClassParent extends Component{
    constructor(){
        super()
        console.log("Parent Constructor")
    }
    render(){
        console.log("Parent Render");
        return(
            <div>
                ClassParent
            </div>
        )
    }
    componentDidMount(){
    console.log("Parent componentDidMount");
    this.setState({name:"Oliver"})
    }
}

export default ClassParent;