import React,{Component} from 'react'


class Heading extends Component{
    constructor(name,age,nationality){
        super(name,age,nationality)
        this.state = {
            name:"John"
        }
        this.handleOnClick = this.handleOnClick.bind(this);
    }
    handleOnClick(){
        this.setState({name:"Oliver"})
    }
    render(){
        return(
            // <div>Name is {this.props.name} , Age is {this.props.age}, and nationality is {this.props.nationality}.</div>
            <div>
                <h1>{this.state.name}</h1>
                <button onClick={this.handleOnClick}>Click here</button>
            </div>
        )
    }
}

export default Heading;