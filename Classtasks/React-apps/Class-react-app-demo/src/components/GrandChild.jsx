import React, { useContext } from 'react'
import { DataContext } from './Parent'

const GrandChild = () => {
    const [data,setdata] = useContext(DataContext);
  return (
    <div>
        <h1>{data}</h1>
        <button onClick={()=>setdata(data+1)}>Increment</button>
        <button onClick={()=>setdata(data-1)}>Decrement</button>
        <button onClick={()=>setdata(0)}>Reset</button>
        <button onClick={()=>setdata(data+2)}>IncrementTwice</button>
        <button onClick={()=>setdata(data+3)}>IncrementThrice</button>
    </div>
  )
}

export default GrandChild