const button = document.querySelector("#button-for-fetch");
const table = document.querySelector('#tab')

button.addEventListener("click",handleClick)

function handleClick(event){
    event.preventDefault();
    
    fetch('https://jsonplaceholder.typicode.com/todos/1')
    .then(response => response.json())
    .then(res => {
        console.log(res.data);
        for(let i=0;i<res.data.length;i++){
            const tr = document.createElement("tr");
            const td1 = document.createElement("td");
            const td2 = document.createElement("td");
            const td3 = document.createElement("td");
            const td4 = document.createElement("td");
            td1.innerText = res.data[i].userid;
            td2.innerText = res.data[i].id;
            td3.innerText = res.data[i].title;
            td4.innerText = res.data[i].completed;

            tr.append(td1, td2, td3, td4);

            table.append(tr)
        }
    })
}